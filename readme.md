# Schrödinger's Bug
### Or: What I get for working on a Friday the 13th

During this project, I discovered a bug in Firefox for Linux that goes away when you  examine it in dev tools.

Dev tools closed:

![DEV TOOLS CLOSED](img/devtools-closed.png "dev tools closed")

Dev tools open:

![DEV TOOLS OPEN](img/devtools-open.png "dev tools open")
